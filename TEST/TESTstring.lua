function TESTtrimWS()
   local S = " Life "
   assert(S:trimWS()== "Life")
end

function TESTtrimLWS()
   local S = " Life "
   assert(S:trimLWS()== "Life ")
end

function TESTtrimRWS()
   local S = " Life "
   assert(S:trimRWS()== " Life")
end

function TESTbreakage()
   assert(1==0, "1 does not equal 0")
end
