function UNITcollectTests(Prefix)
   local Collection = {}
   for K, V in pairs(_G) do
      if K:sub(1, #Prefix) == Prefix then
         Collection[K] = V   
      end
   end
   return Collection
end

function UNITrunTests(Collection)
   local Result = 'Running ' ..#Collection.. " tests.\n" 
   local ErrorList = {}
   local SuccessList = {}
   for K, V in pairs(Collection) do
      local Success, Error = pcall(V)
      if not Success then
         ErrorList[#ErrorList+1] = K..": "..Error
      else
         SuccessList[#SuccessList+1] = K
      end 
   end
   Result = Result.."Successful tests:\n"..table.concat(SuccessList,"\n" )
   if (#ErrorList > 0) then
      Result = Result.."\n\nFailed tests:\n"..table.concat(ErrorList, "\n")
   end
   return Result
end

