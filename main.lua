require "UNIT/UNITapp"
require "TEST/TESTstring"

function main(data)
   local T = UNITcollectTests("TEST")
   local R = UNITrunTests(T)
   component.setStatus{data=R:gsub("\n", "<br>")}
end

main()